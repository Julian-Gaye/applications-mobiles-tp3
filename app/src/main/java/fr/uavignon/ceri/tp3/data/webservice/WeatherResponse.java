package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class WeatherResponse {

    public final List<WeatherForecast> weather=null;
    public final Main main=null;
    public final Clouds clouds=null;
    public final Wind wind=null;


    public static class Main {

        public final float temp;
        public final int humidity;

        public Main(float temp, int humidity) {
            this.temp = temp;
            this.humidity = humidity;
        }
    }

    public static class Clouds {

        public final int all;

        public Clouds(int all) {
            this.all = all;
        }
    }

    public static class Wind {

        public final float speed;
        public final int deg;

        public Wind(float speed, int deg) {
            this.speed = speed;
            this.deg = deg;
        }
    }
}
