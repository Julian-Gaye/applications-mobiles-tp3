package fr.uavignon.ceri.tp3.data.webservice;

public class WeatherForecast {

    public final String description;
    public final String icon;

    public WeatherForecast(String description, String icon) {

        this.description = description;
        this.icon = icon;
    }
}
